// Product
TweenMax.staggerTo([$('.product-wrapper')], 1.5, {
	rotation: '-4deg',
	transformOrigin: '50% 100%',
	repeat: -1,
	yoyo: true
}, 1);

// Pterosaur
TweenMax.staggerTo($('.pterosaur-wrapper'), 2, {top: '-280px', repeat: -1, yoyo: true}, 1);

// Earth
var miniWorld = TweenMax.to($('.earth'), 30, {
	rotation: '360deg',
	repeat: -1,
	ease: Power0.easeNone
});

$('#chapter-selection-earth').on('mouseenter', function () {
	miniWorld.timeScale(0);
}).on('mouseleave', function () {
	miniWorld.timeScale(1);
});

// Character 1
var char1Hand = TweenMax.to($('.character-1__hand'), 2, {
	rotation: '10deg',
	repeat: -1,
	yoyo: true
});
char1Hand.delay(1);
var char1LeftLeg = TweenMax.to($('.character-1__left-leg'), 2, {
	rotation: '-5deg',
	repeat: -1,
	yoyo: true
});
char1LeftLeg.delay(1);
var char1RightLeg = TweenMax.to($('.character-1__right-leg'), 2, {
	rotation: '-5deg',
	repeat: -1,
	yoyo: true
});
char1RightLeg.delay(2);
$('.character-1').on('mouseenter', function () {
	char1Hand.timeScale(6);
	char1LeftLeg.timeScale(6);
	char1RightLeg.timeScale(6);
}).on('mouseleave', function () {
	char1Hand.timeScale(1);
	char1LeftLeg.timeScale(1);
	char1RightLeg.timeScale(1);
});

// Character 2
var char2LeftHand = TweenMax.to($('.character-2__left-hand'), 2, {rotation: '-10deg', repeat: -1, yoyo: true});
var char2RightHand = TweenMax.to($('.character-2__right-hand'), 2, {
	rotation: '-10deg',
	repeat: -1,
	yoyo: true
});
var char2LeftLeg = TweenMax.to($('.character-2__left-leg'), 2, {rotation: '-10deg', repeat: -1, yoyo: true});
var char2RightLeg = TweenMax.to($('.character-2__right-leg'), 2, {rotation: '-10deg', repeat: -1, yoyo: true});
$('.character-2').on('mouseenter', function () {
	char2LeftHand.timeScale(6);
	char2RightHand.timeScale(7);
	char2LeftLeg.timeScale(6);
	char2RightLeg.timeScale(6);
}).on('mouseleave', function () {
	char2LeftHand.timeScale(1);
	char2RightHand.timeScale(1);
	char2LeftLeg.timeScale(1);
	char2RightLeg.timeScale(1);
});

// Character 3
var char3 = TweenMax.to($('.character-3__leg'), 2, {
	rotation: '-10deg',
	transformOrigin: '80% 0%',
	repeat: -1,
	yoyo: true
});
$('.character-3').on('mouseenter', function () {
	char3.timeScale(6);
}).on('mouseleave', function () {
	char3.timeScale(1);
});
